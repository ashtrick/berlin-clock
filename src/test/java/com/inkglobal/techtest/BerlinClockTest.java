package com.inkglobal.techtest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.inkglobal.techtest.exception.InvalidInputException;

public class BerlinClockTest {
	
	BerlinClock berlinClock = null;
	
	@Before
	public void setup() {
		berlinClock = new BerlinClock();
	}
	
	// Should throw error for hh:mm time format
	@Test (expected = InvalidInputException.class)
	public void timeOmittingSecondsShouldThrowInvalidInputException() throws InvalidInputException {
		berlinClock.convertToBerlinTime("16:37");
	}
	
	// Should throw error for non-numeric time value
	@Test (expected = InvalidInputException.class)
	public void nonNumericTimeValueShouldThrowInvalidInputException() throws InvalidInputException {
		berlinClock.convertToBerlinTime("16:TT:00");
	}
	
	// Should throw error for time values out of range 
	// Example: >24 for hour, > 59 for minute etc..
	@Test (expected = InvalidInputException.class)
	public void outOfRangeTimeValuesShouldThrowInvalidInputException() throws InvalidInputException {
		berlinClock.convertToBerlinTime("25:15:14");
	}
	
    // Yellow lamp should blink on/off every two seconds
    @Test
    public void yellowLampShouldBlinkOnOffEveryTwoSeconds() {
        assertEquals("Yellow lamp does not switch on for 0 second", "Y", berlinClock.getSeconds(0));
        assertEquals("Yellow lamp switches on for 1 second", "O", berlinClock.getSeconds(1));
        assertEquals("Yellow lamp does not switch on for 2 second", "Y", berlinClock.getSeconds(2));
        assertEquals("Yellow lamp switches on for 59 second", "O", berlinClock.getSeconds(59));
    }

    // Top hours should have 4 lamps
    @Test
    public void topHoursShouldHave4Lamps() {
        assertEquals("Top hours does not have 4 lamps", 4, berlinClock.getTopHours(7).length());
    }

    // Top hours should light a red lamp for every 5 hours
    @Test
    public void topHoursShouldLightRedLampForEvery5Hours() {
        assertEquals("Top hours lamp test failed for 0 hours", "OOOO", berlinClock.getTopHours(0));
        assertEquals("Top hours lamp test failed for 13 hours", "RROO", berlinClock.getTopHours(13));
        assertEquals("Top hours lamp test failed for 23 hours", "RRRR", berlinClock.getTopHours(23));
        assertEquals("Top hours lamp test failed for 24 hours", "RRRR", berlinClock.getTopHours(24));
    }

    // Bottom hours should have 4 lamps
    @Test
    public void bottomHoursShouldHave4Lamps() {
        assertEquals("Bottom hours does not have 4 lamps", 4, berlinClock.getBottomHours(5).length());
    }

    // Bottom hours should light a red lamp for every hour left from top hours
    @Test
    public void bottomHoursShouldLightRedLampForEveryHourLeftFromTopHours() {
        assertEquals("Bottom hours lamp test failed for 0 hours", "OOOO", berlinClock.getBottomHours(0));
        assertEquals("Bottom hours lamp test failed for 13 hours", "RRRO", berlinClock.getBottomHours(13));
        assertEquals("Bottom hours lamp test failed for 23 hours", "RRRO", berlinClock.getBottomHours(23));
        assertEquals("Bottom hours lamp test failed for 24 hours", "RRRR", berlinClock.getBottomHours(24));
    }

    // Top minutes should have 11 lamps
    @Test
    public void topMinutesShouldHave11Lamps() {
        assertEquals("Top minutes does not have 11 lamps", 11, berlinClock.getTopMinutes(34).length());
    }

    // Top minutes should have 3rd, 6th and 9th lamps in red to indicate first quarter, half and last quarter
    @Test
    public void topMinutesShouldHave3rd6thAnd9thLampsInRedToIndicateFirstQuarterHalfAndLastQuarter() {
        String minutes32 = berlinClock.getTopMinutes(32);
        assertEquals("Top minutes does not have 3rd lamp Red", "R", minutes32.substring(2, 3));
        assertEquals("Top minutes does not have 6th lamp Red", "R", minutes32.substring(5, 6));
        assertEquals("Top minutes does not have 9th lamp Red", "O", minutes32.substring(8, 9));
    }

    // Top minutes should light a yellow lamp for every 5 minutes unless it's first quarter, half or last quarter
    @Test
    public void topMinutesShouldLightYellowLampForEvery5MinutesUnlessItIsFirstQuarterHalfOrLastQuarter() {
        assertEquals("Top minutes does not have Yellow lamp check for minute 0", "OOOOOOOOOOO", berlinClock.getTopMinutes(0));
        assertEquals("Top minutes does not have Yellow lamp check for minute 17", "YYROOOOOOOO", berlinClock.getTopMinutes(17));
        assertEquals("Top minutes does not have Yellow lamp check for minute 59", "YYRYYRYYRYY", berlinClock.getTopMinutes(59));
    }

    // Bottom minutes should have 4 lamps
    @Test
    public void bottomMinutesShouldHave4Lamps() {
        assertEquals("Bottom minutes does not have 4 lamps", 4, berlinClock.getBottomMinutes(0).length());
    }

    // Bottom minutes should light a yellow lamp for every minute left from top minutes
    @Test
    public void bottomMinutesShouldLightYellowLampForEveryMinuteLeftFromTopMinutes() {
        assertEquals("Bottom minutes failed for minute 0", "OOOO", berlinClock.getBottomMinutes(0));
        assertEquals("Bottom minutes failed for minute 17", "YYOO", berlinClock.getBottomMinutes(17));
        assertEquals("Bottom minutes failed for minute 59", "YYYY", berlinClock.getBottomMinutes(59));
    }

    // Berlin Clock should result in array with 5 elements
    @Test
    public void berlinClockShouldResultInArrayWith5Elements()  {
        try {
			assertEquals("Check for array with 5 elements failed", 5, berlinClock.convertToBerlinTime("13:17:01").length);
		} catch (InvalidInputException e) {
			fail("[berlinClockShouldResultInArrayWith5Elements]InvalidInputException thrown for valid input");
		}
    }

    // Berlin Clock should "result in correct seconds, hours and minutes" 
    @Test
    public void berlinClockShouldResultInCorrectSecondsHoursAndMinutes() {
    	try{
	        String[] berlinTime = berlinClock.convertToBerlinTime("16:37:16");
	        String[] expected = new String[] {"Y", "RRRO", "ROOO", "YYRYYRYOOOO", "YYOO"};
	        assertEquals("Check for correct array length failed", expected.length, berlinTime.length);
	        for (int index = 0; index < expected.length; index++) {
	            assertEquals("Expected correct value check failed", expected[index], berlinTime[index]);
	        }
    	}catch (InvalidInputException e) {
			fail("[berlinClockShouldResultInCorrectSecondsHoursAndMinutes]InvalidInputException thrown for valid input");
		}   
    }
    
    // Test with below values:
    //
	//    Input       Result
	//    00:00:00    Y
	//                OOOO
	//                OOOO
	//                OOOOOOOOOOO
	//                OOOO
	//
	//    13:17:01    O
	//                RROO
	//                RRRO
	//                YYROOOOOOOO
	//                YYOO
	//
	//    23:59:59    O
	//                RRRR
	//                RRRO
	//                YYRYYRYYRYY
	//                YYYY
	//
	//    24:00:00    Y
	//                RRRR
	//                RRRR
	//                OOOOOOOOOOO
	//                OOOO
    @Test
    public void berlinClockShouldResultInCorrectSecondsHoursAndMinutesForAllTests() {
    	try{
    		// 00:00:00
    		String[] berlinTime = berlinClock.convertToBerlinTime("00:00:00");
	        String[] expected = new String[] {"Y", "OOOO", "OOOO", "OOOOOOOOOOO", "OOOO"};
	        assertEquals("Check for correct array length failed for 00:00:00", expected.length, berlinTime.length);
	        for (int index = 0; index < expected.length; index++) {
	            assertEquals("Expected correct value check failed for 00:00:00", expected[index], berlinTime[index]);
	        }
	        
	        berlinTime = berlinClock.convertToBerlinTime("13:17:01");
	        expected = new String[] {"O", "RROO", "RRRO", "YYROOOOOOOO", "YYOO"};
	        assertEquals("Check for correct array length failed for 13:17:01", expected.length, berlinTime.length);
	        for (int index = 0; index < expected.length; index++) {
	            assertEquals("Expected correct value check failed for 13:17:01", expected[index], berlinTime[index]);
	        }
	        
	        berlinTime = berlinClock.convertToBerlinTime("23:59:59");
	        expected = new String[] {"O", "RRRR", "RRRO", "YYRYYRYYRYY", "YYYY"};
	        assertEquals("Check for correct array length failed for 23:59:59", expected.length, berlinTime.length);
	        for (int index = 0; index < expected.length; index++) {
	            assertEquals("Expected correct value check failed for 23:59:59", expected[index], berlinTime[index]);
	        }
	        
	        berlinTime = berlinClock.convertToBerlinTime("24:00:00");
	        expected = new String[] {"Y", "RRRR", "RRRR", "OOOOOOOOOOO", "OOOO"};
	        assertEquals("Check for correct array length failed for 24:00:00", expected.length, berlinTime.length);
	        for (int index = 0; index < expected.length; index++) {
	            assertEquals("Expected correct value check failed for 24:00:00", expected[index], berlinTime[index]);
	        }
	        
    	}catch (InvalidInputException e) {
			fail("[berlinClockShouldResultInCorrectSecondsHoursAndMinutes]InvalidInputException thrown for valid input");
		}   
    }
	
}
