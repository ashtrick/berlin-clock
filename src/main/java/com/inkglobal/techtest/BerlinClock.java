package com.inkglobal.techtest;

import java.util.ArrayList;
import java.util.List;

import com.inkglobal.techtest.exception.InvalidInputException;

public class BerlinClock {
	
	/**
	 * This method accepts the time in hh:mm:ss format
	 * and returns a string array containing the time
	 * in Berlin format
	 *  
	 * @param time String time in hh:mm:ss format
	 * @return String[] in Berlin time
	 */
	public String[] convertToBerlinTime(String time) throws InvalidInputException {
        
		// Validate input time
		if(!validInputTime(time)) {
			throw new InvalidInputException("Invalid input time. Please input time in hh:mm:ss format.");
		}
		
        List<Integer> parts = new ArrayList<>();

        // Split the input time
        for (String part : time.split(":")) {
            parts.add(Integer.parseInt(part));
        }
        
        // Calculate Berlin time
        return new String[] {
                getSeconds(parts.get(2)),
                getTopHours(parts.get(0)),
                getBottomHours(parts.get(0)),
                getTopMinutes(parts.get(1)),
                getBottomMinutes(parts.get(1))
        };
    }

	/**
	 * Validates the input time
	 * 
	 * @param time
	 * @return
	 */
	private boolean validInputTime(String time) {
		String[] timeArr = time.split(":");
		List<Integer> parts = new ArrayList<>();
		
		// Check the format as hh:mm:ss
		if(null == timeArr || timeArr.length != 3) {
			return false;
		}
		
		// Check if all are numbers
		try {
			for(String part : timeArr) {
				parts.add(Integer.parseInt(part));
			}
		} catch (NumberFormatException e) {
			return false;
		}
		
		// Check if time values are in within range
		if((parts.get(0) < 0 || parts.get(0) > 24)
				|| parts.get(1) < 0 || parts.get(1) > 59
				|| parts.get(2) < 0 || parts.get(2) > 59) {
			return false;
		}
		
		
		return true;
	}

	/**
	 * Get the value for the lamp in the top of the clock
	 * depending on the value(seconds) passed to the method
	 * 
	 * This is a yellow lamp that blinks on/off every two seconds
	 *  
	 * @param secondsValue Seconds value
	 * @return Value of the yellow lamp on top of the clock
	 */
    protected String getSeconds(int secondsValue) {
    	// Checks if the seconds is multiple of 2
    	// If yes, return Y else O
        if (secondsValue % 2 == 0) {
        	return "Y";
        } else {
        	return "O";
        }
    }

    /**
     * Gets the value for the top row hours of the clock
     * 
     * @param hourValue
     * @return
     */
    protected String getTopHours(int hourValue) {
    	// This method invokes the getNumberOfSignsInTopRow method
    	// to get number of active lamps in the top row of hours.
    	// Then pass the value to getOnOff method along with number of
    	// lamps and colour of lamp (R) which will return the lamps in order
        return getOnOff(4, getNumberOfOnSignsInTopRow(hourValue), "R");
    }

    /**
     * Gets the value for the bottom row hours of the clock
     * 
     * @param hourValue
     * @return
     */
    protected String getBottomHours(int hourValue) {
    	// Get the number of lamps to be activated in the bottom row
    	// of hours (hourValue % 5) and pass the value to getOnOff 
    	// method along with the number of lamps and colour of lamp (R)
    	// which will return the lamps in order
        return getOnOff(4, hourValue % 5, "R");
    }

    /**
     * Gets the value for the top row minutes of the clock
     * 
     * @param minutesValue
     * @return
     */
    protected String getTopMinutes(int minutesValue) {
    	// This method invokes getNumberOfSignsInTopRow method to get the
    	// number of active lamps in the top row of minutes.
    	// Then pass the value to getOnOff method along with number of
    	// lamps and colour of lamp (Y) which will return the lamps in order
    	// Then replaces the each quarter lamp valur with R
        return getOnOff(11, getNumberOfOnSignsInTopRow(minutesValue), "Y").replaceAll("YYY", "YYR");
    }

    /**
     * Gets the value for the bottom row minutes of the clock
     * 
     * @param minutesValue
     * @return
     */
    protected String getBottomMinutes(int minutesValue) {
    	// Get the number of lamps to be activated in the bottom row
    	// of minutes (minutesValue % 5) and pass the value to getOnOff 
    	// method along with the number of lamps and colour of lamp (Y)
    	// which will return the lamps in order
        return getOnOff(4, minutesValue % 5, "Y");
    }
    
    /**
     * Gets the lamps activated with colour of the lamp passed to the method
     * 
     * This method assigns the activated lamp colours to rowOfLamps iterating through
     * onSigns and then iterate through the remaining lamps to assign valur O
     * 
     * @param lamps Number of lamps in the row
     * @param onSigns Number of activated lamps
     * @param onSign Sign or colour of the activated lamp
     * @return String representing row of lamps
     */
    private String getOnOff(int lamps, int onSigns, String onSign) {
        StringBuilder rowOfLamps = new StringBuilder();

        // Iterate through the number of active lamps and 
        // assign the sign or colour
        for (int i = 0; i < onSigns; i++) {
            rowOfLamps.append(onSign);
        }
        
        // Iterate through the remaining lamps other than
        // active and assign 'O' to them
        for (int i = 0; i < (lamps - onSigns); i++) {
            rowOfLamps.append("O");
        }
        
        return rowOfLamps.toString();
    }
 
    /**
     * This method returns number of activated lamps in the top row
     * of hours and minutes
     * 
     * @param value Value of hour or minute
     * @return Number of activated lamps
     */
    private int getNumberOfOnSignsInTopRow(int value) {
    	// Gets the integer value divided by 5 as each lamp 
    	// in the top row for hours and minutes represents
    	// 5 hours and 5 minutes respectively
    	return (value / 5);
    }

    
    /**
     * Want to try it out how it works?? 
     * Run it with the commandline argument as the time
     * and see the result.
     * 
     * @param args
     */
    public static void main(String[] args) {
		
    	BerlinClock berlinClock = new BerlinClock();
    	
    	if(args.length < 1) {
    		System.out.println("Please pass the time as command line argument");
    		System.exit(0);
    	}

    	try { 
	    	String[] berlinTime = berlinClock.convertToBerlinTime(args[0]);
	    	System.out.println("Input: " + args[0]);
	    	System.out.println("Output: ");
	        for (int index = 0; index < berlinTime.length; index++) {
	        	
	            System.out.println("\t" + berlinTime[index]);
	        }
    	} catch (Exception e) {
    		System.err.println("Error: " +  e.getLocalizedMessage());
    	}
    	
	}
}
